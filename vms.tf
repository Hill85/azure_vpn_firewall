resource "azurerm_network_interface" "vm_nic" {
  name                = "vm-nic"
  location            = azurerm_resource_group.desafio_fire.location
  resource_group_name = azurerm_resource_group.desafio_fire.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet_internal.id
    private_ip_address_allocation = "Dynamic"
    #public_ip_address_id = azurerm_public_ip.vm_public_ip.id
  }
}

resource "azurerm_windows_virtual_machine" "win10" {
  name                = "win10"
  resource_group_name = azurerm_resource_group.desafio_fire.name
  location            = azurerm_resource_group.desafio_fire.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  admin_password      = var.pass
  network_interface_ids = [
    azurerm_network_interface.vm_nic.id,
  ]
  
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsDesktop"
    offer     = "windows-10"
    sku       = "19h2-pro-g2"
    version   = "latest"
  }

  
}