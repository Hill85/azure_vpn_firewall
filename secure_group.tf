
/*

# Regras de entrada 

resource "azurerm_network_security_group" "SG_entrada" {
  name                = "Security_group_Inbound"
  location            = azurerm_resource_group.desafio_fire.location
  resource_group_name = azurerm_resource_group.desafio_fire.name

  security_rule {
    name                       = "Allow_3389"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "3389"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"


  }

  tags = {
    environment = "Production"
  }

}

# Regras de saída 

resource "azurerm_network_security_rule" "SG_saida" {
  name                        = "SG_saida"
  priority                    = 100
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.desafio_fire.name
  network_security_group_name = azurerm_network_security_group.SG_entrada.name
}

*/