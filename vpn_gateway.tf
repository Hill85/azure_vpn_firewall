

resource "azurerm_subnet" "vpn_gateway_subnet" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.desafio_fire.name
  virtual_network_name = azurerm_virtual_network.vnet_desafio_fire.name
  address_prefixes     = ["10.0.10.0/24"]
}

resource "azurerm_public_ip" "vpn_public_ip" {
  name                = "test"
  location            = azurerm_resource_group.desafio_fire.location
  resource_group_name = azurerm_resource_group.desafio_fire.name

  allocation_method = "Dynamic"
}

resource "azurerm_virtual_network_gateway" "vpn_gateway" {
  depends_on = [
    azurerm_subnet.vpn_gateway_subnet, azurerm_public_ip.vpn_public_ip
  ]
  name                = "test"
  location            = azurerm_resource_group.desafio_fire.location
  resource_group_name = azurerm_resource_group.desafio_fire.name

  type     = "Vpn"
  vpn_type = "RouteBased"

  active_active = false
  enable_bgp    = false
  sku           = "VpnGw1"

  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.vpn_public_ip.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.vpn_gateway_subnet.id
  }

  vpn_client_configuration {
    address_space = ["10.2.0.0/24"]
    vpn_client_protocols  = ["SSTP","IkeV2"]
    root_certificate {
      name = "P2SRootCert"

      public_cert_data = file("rootcertificate-201022-172846.txt")

    }

  }
}