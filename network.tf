
# Rede virtual

resource "azurerm_virtual_network" "vnet_desafio_fire" {
  name                = "testvnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.desafio_fire.location
  resource_group_name = azurerm_resource_group.desafio_fire.name
}

# Subnet da rede interna

resource "azurerm_subnet" "subnet_internal" {
  name                 = "subnet_internal"
  resource_group_name  = azurerm_resource_group.desafio_fire.name
  virtual_network_name = azurerm_virtual_network.vnet_desafio_fire.name
  address_prefixes     = ["10.0.1.0/24"]
}


#Subnet do firewall

resource "azurerm_subnet" "AzureFirewallSubnet" {
  name                 = "AzureFirewallSubnet"
  resource_group_name  = azurerm_resource_group.desafio_fire.name
  virtual_network_name = azurerm_virtual_network.vnet_desafio_fire.name
  address_prefixes     = ["10.0.20.0/26"]

}

#IP público para VM

resource "azurerm_public_ip" "vm_public_ip" {
  name                  = "VM-public_ip"
  resource_group_name   = azurerm_resource_group.desafio_fire.name
  location              = azurerm_resource_group.desafio_fire.location
  allocation_method     = "Static"
  sku                   = "Standard"
}

#IP público para o firewall

resource "azurerm_public_ip" "azure_firewall_public_ip" {
  name                  = "FW-public_ip"
  resource_group_name   = azurerm_resource_group.desafio_fire.name
  location              = azurerm_resource_group.desafio_fire.location
  allocation_method     = "Static"
  sku                   = "Standard"
}

# Azure firewall

resource "azurerm_firewall" "firewall_desafio_fire" {
 depends_on = [
    azurerm_public_ip.azure_firewall_public_ip, azurerm_windows_virtual_machine.win10 
  ]
  name                = "testfirewall"
  location            = azurerm_resource_group.desafio_fire.location
  resource_group_name = azurerm_resource_group.desafio_fire.name

  ip_configuration {
    name                 = "configuration"
    subnet_id            = azurerm_subnet.AzureFirewallSubnet.id
    public_ip_address_id = azurerm_public_ip.azure_firewall_public_ip.id
  }
}

# Regra para filtro de sites 

resource "azurerm_firewall_application_rule_collection" "azure_firewall" {
  name                = "azure_firewall"
  azure_firewall_name = azurerm_firewall.firewall_desafio_fire.name
  resource_group_name = azurerm_resource_group.desafio_fire.name
  priority            = 100
  action              = "Allow"

  rule {
    name = "Facebook"

    source_addresses = [
      "10.0.1.0/24",
    ]

    target_fqdns = [
      "*.facebook.com","*.fbcdn.net","facebook.com",
    ]

    protocol {
      port = "443"
      type = "Https"
    }
  }

  rule {
    name = "Instagram"

    source_addresses = [
      "10.0.1.0/24",
    ]

    target_fqdns = [
      "*.instagram.com","instagram.com",
    ]

    protocol {
      port = "443"
      type = "Https"
    }
  }
}

#tabela de rota interna 
resource "azurerm_route_table" "RT-interno" {
  name                          = "RT-interno"
  location                      = azurerm_resource_group.desafio_fire.location
  resource_group_name           = azurerm_resource_group.desafio_fire.name
  disable_bgp_route_propagation = false

  route {
    name           = "route1"
    address_prefix = "0.0.0.0/0"
    next_hop_type  = "VirtualAppliance"
    next_hop_in_ip_address = azurerm_firewall.firewall_desafio_fire.ip_configuration[0].private_ip_address
  }

  
}

# associação da RT

resource "azurerm_subnet_route_table_association" "RT-association" {
  subnet_id      = azurerm_subnet.subnet_internal.id
  route_table_id = azurerm_route_table.RT-interno.id
}


